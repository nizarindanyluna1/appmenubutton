package com.example.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dbFragment

class salirFragment : Fragment() {
    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        rcvLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)

        rcvLista.layoutManager = LinearLayoutManager(requireContext())
        val listaAlumno = ArrayList<AlumnoLista>()
        listaAlumno.add(AlumnoLista(1, "202003100", "LIZARRAGA CAMACHO JESUS ARMANDO", "", "ING. TEC. INFORMACION", R.mipmap.a2021030262))
        listaAlumno.add(AlumnoLista(2, "202003101", "LOPEZ ACOSTA JOSE LUIS", "", "ING. TEC. INFORMACION", R.mipmap.a2021030262))
        listaAlumno.add(AlumnoLista(3, "202003102", "NARANJO LOPEZ MARIA ISABEL", "", "ING. TEC. INFORMACION", R.mipmap.a1))
        listaAlumno.add(AlumnoLista(4, "202003103", "LIZARRAGA LOPEZ MARIO DE JESUS", "", "ING. TEC. INFORMACION", R.mipmap.a2021030262))
        listaAlumno.add(AlumnoLista(5, "202003104", "LOPEZ ACOSTA MIGUEL MAURICIO", "", "ING. TEC. INFORMACION", R.mipmap.a2021030262))
        listaAlumno.add(AlumnoLista(6, "202003105", "NARANJO LOPEZ CARLOS EDUARDO", "", "ING. TEC. INFORMACION", R.mipmap.a2021030262))
        listaAlumno.add(AlumnoLista(7, "202003106", "FERNANDEZ LOPEZ MARCO", "", "ING. TEC. INFORMACION", R.mipmap.a2020030847))
        listaAlumno.add(AlumnoLista(8, "202003107", "DOMINGUEZ ACOSTA JOSE MIGUEL", "", "ING. TEC. INFORMACION", R.mipmap.a2020030321))
        listaAlumno.add(AlumnoLista(9, "202003108", "ACOSTA GARCIA LUIS MIGUEL", "", "ING. TEC. INFORMACION", R.mipmap.a2020030321))

        adaptador = MiAdaptador(listaAlumno, requireContext())
        rcvLista.adapter = adaptador

        btnNuevo.setOnClickListener {
            cambiarDbFragment()
        }

        adaptador.setOnClickListener({
            val pos: Int = rcvLista.getChildAdapterPosition(it)

            //Obtener el objeto
            val alumno: AlumnoLista
            alumno = listaAlumno[pos]

            //agrgar el objeto
            val bundle = Bundle().apply {

                putSerializable("mialumno", alumno)
            }

            //asignar al fragments
            val dbFragment = dbFragment()
            dbFragment.arguments = bundle

            //llamar al fragments destino
            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        })

        return view
    }

    private fun cambiarDbFragment() {
        val cambioFragment = fragmentManager?.beginTransaction()
        cambioFragment?.replace(R.id.frmContenedor, dbFragment())
        cambioFragment?.addToBackStack(null)
        cambioFragment?.commit()
    }
}
