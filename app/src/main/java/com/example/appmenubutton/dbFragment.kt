import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.appmenubutton.AlumnoLista
import com.example.appmenubutton.R
import com.example.appmenubutton.database.Alumno
import com.example.appmenubutton.database.dbAlumnos

class dbFragment : Fragment() {

    private lateinit var db: dbAlumnos
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: TextView
    //private lateinit var imgAlumno: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.lblUrlImg)
        //imgAlumno = view.findViewById(R.id.imgAlumno)

        arguments?.let {
            val alumnoLista = it?.getSerializable("mialumno") as AlumnoLista

            txtNombre.setText(alumnoLista.nombre)
            txtDomicilio.setText(alumnoLista.domicilio)
            txtEspecialidad.setText(alumnoLista.especialidad)
            txtMatricula.setText(alumnoLista.matricula)
            txtUrlImagen.setText(alumnoLista.foto)
            //imgAlumno.setImageResource(alumnoLista.foto)
        }
        db = dbAlumnos(requireContext())


        btnGuardar.setOnClickListener {
            if (txtNombre.text.toString().isEmpty() || txtDomicilio.text.toString().isEmpty() ||
                txtMatricula.text.toString().isEmpty() || txtEspecialidad.text.toString().isEmpty()
            ) {
                Toast.makeText(requireContext(), "Faltó información por capturar", Toast.LENGTH_SHORT).show()
            } else {
                val alumno = Alumno().apply {
                    nombre = txtNombre.text.toString()
                    matricula = txtMatricula.text.toString()
                    domicilio = txtDomicilio.text.toString()
                    especialidad = txtEspecialidad.text.toString()
                    foto = "Pendiente"
                }

                try {
                    db = dbAlumnos(requireContext())
                    db.openDataBase()
                    val existingAlumno = db.getAlumno(alumno.matricula)
                    if (existingAlumno.id != 0) {
                        val rowsUpdated = db.ActualizarAlumno(alumno, existingAlumno.id)
                        if (rowsUpdated > 0) {
                            Toast.makeText(requireContext(), "Alumno actualizado con éxito", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(requireContext(), "No se pudo actualizar el alumno", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        val id: Long = db.InsertarAlumno(alumno)
                        Toast.makeText(requireContext(), "Se agregó con éxito el ID: $id", Toast.LENGTH_SHORT).show()
                    }
                    db.close()
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Faltó capturar la matrícula", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    db = dbAlumnos(requireContext())
                    db.openDataBase()
                    val alumno = db.getAlumno(txtMatricula.text.toString())
                    if (alumno.id != 0) {
                        txtNombre.setText(alumno.nombre)
                        txtDomicilio.setText(alumno.domicilio)
                        txtEspecialidad.setText(alumno.especialidad)
                        btnBorrar.isEnabled = true
                        Toast.makeText(requireContext(), "Alumno encontrado", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                    }
                    db.close()
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
        }

        btnBorrar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(requireContext(), "Faltó capturar la matrícula", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    db = dbAlumnos(requireContext())
                    db.openDataBase()
                    val alumno = db.getAlumno(txtMatricula.text.toString())
                    if (alumno.id != 0) {
                        val rowsDeleted = db.BorrarAlumnos(alumno.id)
                        if (rowsDeleted > 0) {
                            Toast.makeText(requireContext(), "Alumno eliminado correctamente", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                            btnBorrar.isEnabled = false
                        } else {
                            Toast.makeText(requireContext(), "No se pudo eliminar el alumno", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                    }
                    db.close()
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
        }

        return view
    }

    private fun limpiarCampos() {
        txtMatricula.setText("")
        txtNombre.setText("")
        txtDomicilio.setText("")
        txtEspecialidad.setText("")
        txtUrlImagen.setText("")
    }
}
