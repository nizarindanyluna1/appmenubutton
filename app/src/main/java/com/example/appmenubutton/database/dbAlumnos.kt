package com.example.appmenubutton.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf

@Suppress("NAME_SHADOWING", "UNREACHABLE_CODE")
class dbAlumnos (private val context: Context) {

    private var dbHelper: AlumnosDBHelper = AlumnosDBHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirTabla.Alumnos.ID,
        DefinirTabla.Alumnos.MATRICULA,
        DefinirTabla.Alumnos.NOMBRE,
        DefinirTabla.Alumnos.DOMICILIO,
        DefinirTabla.Alumnos.ESPECIALIDAD,
        DefinirTabla.Alumnos.FOTO
    )
    fun openDataBase() {
        db = dbHelper.writableDatabase
    }
    fun InsertarAlumno(alumno: Alumno): Long {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.insert(DefinirTabla.Alumnos.TABLA, null, valores)
    }
    fun ActualizarAlumno(alumno: Alumno, id: Int): Int {
        val valores = contentValuesOf().apply {
            put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirTabla.Alumnos.FOTO, alumno.foto)
        }
        return db.update(
            DefinirTabla.Alumnos.TABLA, valores,
            "${DefinirTabla.Alumnos.ID}= $id", null
        )
    }
    fun BorrarAlumnos(id:Int):Int {
        return db.delete(DefinirTabla.Alumnos.TABLA,
            "${DefinirTabla.Alumnos.ID}=?"
            , arrayOf(id.toString())
        )
    }
    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }

    }
    fun getAlumno(id: String): Alumno{       //Corregido
        val db = dbHelper.readableDatabase
        val cursor =
            db.query(DefinirTabla.Alumnos.TABLA,leerCampos,"${DefinirTabla.Alumnos.
            MATRICULA} = ?",
                arrayOf(id.toString() ),null,null,null,null)

        if(cursor.moveToFirst()){
            val alumno = mostrarAlumnos(cursor)

            cursor.close()
            return alumno
        }
        else {
            var alumno: Alumno = Alumno()
            return alumno
        }

    }

    fun leerTodos (): ArrayList<Alumno>{
        val cursor =
            db.query(DefinirTabla.Alumnos.TABLA,leerCampos,null,null,null,null,null
            )
        val listaAlumno = ArrayList<Alumno>()
        cursor.moveToFirst()
        while(!cursor.isAfterLast) {
            val alumno = mostrarAlumnos(cursor)
            listaAlumno.add(alumno)
            cursor.moveToNext()
        }
        cursor.close()
        return listaAlumno
    }

    fun close() {
        dbHelper.close()
    }
}
